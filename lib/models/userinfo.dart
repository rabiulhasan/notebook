class UserInfo {
  dynamic id;
  dynamic name;
  dynamic username;
  dynamic password;

  UserInfo({this.id, this.name, this.username, this.password});

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'name': name,
      'username': username,
      'password': password,
    };
  }
}
