import 'dart:io';

import 'package:notebook/constants/appconstants.dart';
import 'package:notebook/models/notebook.dart';
import 'package:notebook/models/userinfo.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as p;

class DatabaseHelper {
  Future<Database> initDatabase() async {
    Directory appDir = await getApplicationDocumentsDirectory();
    String path = p.join(appDir.path, "notebookDb.db");
    return openDatabase(path, version: 7, onCreate: (db, version) async {
       await db.execute(
          'CREATE TABLE ${AppConstants.noteInfoTb} (id INTEGER Primary Key Autoincrement,title TEXT,content TEXT,date TEXT)');
      await db.execute(
          'CREATE TABLE ${AppConstants.userInfoTb} (id INTEGER Primary key Autoincrement,name TEXT,username TEXT,password TEXT)');
    });
  }

  Future<int> addUser(UserInfo userInfo) async {
    Database db = await initDatabase();
    return db.insert(AppConstants.userInfoTb, userInfo.toMap());
  }

  Future<bool> isUserExist(String userName) async {
    Database db = await initDatabase();
    List<Map<String, dynamic>> userMapList =
        await db.rawQuery("Select * From userInfo where username='$userName'");
    List<UserInfo> userList = List.generate(userMapList.length, (index) {
      Map<String, dynamic> mUser = userMapList[index];
      return UserInfo(
        id: mUser['id'],
        name: mUser['name'],
        username: mUser['username'],
        password: mUser['password'],
      );
    });
    if (userList.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  Future<int> insertNoteBook(NoteBook noteBook) async {
    Database db = await initDatabase();
    return db.insert(AppConstants.noteInfoTb, noteBook.toMap());
  }

  Future<List<NoteBook>> fetchNoteBookList() async {
    Database db = await initDatabase();
    List<Map<String, dynamic>> noteBookList =
        await db.query(AppConstants.noteInfoTb);

    return List.generate(noteBookList.length, (index) {
      Map<String, dynamic> mNotebook = noteBookList[index];
      return NoteBook(
        id: mNotebook['id'],
        title: mNotebook['title'],
        content: mNotebook['content'],
        date: mNotebook['date'],
      );
    });
  }

  Future<int> updateNoteBook(NoteBook noteBook) async {
    Database db = await initDatabase();
    return db.update(AppConstants.noteInfoTb, noteBook.toMap(),
        where: 'id = ?', whereArgs: [noteBook.id]);
  }

  Future<int> deleteNoteBook(NoteBook noteBook) async {
    Database db = await initDatabase();
    return db.delete(AppConstants.noteInfoTb,
        where: 'id = ?', whereArgs: [noteBook.id]);
  }

  Future<int> deleteData(int id) async {
    Database db = await initDatabase();
    return db
        .rawDelete('DELETE FROM ${AppConstants.noteInfoTb} WHERE id = $id');
  }

  Future<int> deleteTable(String tableName) async {
    Database db = await initDatabase();
    return db.delete(tableName);
  }
}
