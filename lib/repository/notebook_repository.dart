import 'package:notebook/databasehelper/databasehelper.dart';
import 'package:notebook/models/notebook.dart';

class NoteBookRepository {
  final dbHelper = DatabaseHelper();

  Future<int> insertNote(NoteBook noteBook) {
    return dbHelper.insertNoteBook(noteBook);
  }

  Future<int> updateNote(NoteBook noteBook) {
    return dbHelper.updateNoteBook(noteBook);
  }

  Future<List<NoteBook>> fetchNote()  async{
    return await dbHelper.fetchNoteBookList();
  }

  Future<int> deleteNote(NoteBook noteBook) async {
    return dbHelper.deleteNoteBook(noteBook);
  }

  Future<int> deleteNoteById(int id) async {
    return dbHelper.deleteData(id);
  }

  Future<int> deleteTable(String tableName) async {
    return dbHelper.deleteTable(tableName);
  }
}
