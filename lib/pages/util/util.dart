class Utility{

  String? greeting()
  {
    String message = "";
    int hour = DateTime.now().hour;
    if (hour > 0 && hour <5) {
      message = "Good Night";
    } else if (hour >=5 && hour < 12) {
      message = "Good Morning";
    } else if (hour >=12 && hour < 18) {
      message = "Good Afternoon";
    }else if (hour >=18 && hour < 20) {
      message = "Good Evening";
    }
    else
    {
      message = "Good Night";
    }
    return message;
  }
}