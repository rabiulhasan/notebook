import 'package:flutter/material.dart';
import 'package:notebook/pages/login_page.dart';
import 'package:notebook/pages/registration_page.dart';

class DrawerPage extends StatefulWidget {
  const DrawerPage({Key? key}) : super(key: key);

  @override
  State<DrawerPage> createState() => _DrawerPageState();
}

class _DrawerPageState extends State<DrawerPage> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          const DrawerHeader(
            decoration: BoxDecoration(color: Colors.grey),
            child: Center(
                child: Text(
              'Note Book',
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
              ),
            )),
          ),
          ListTile(
            title: const Text('Registration'),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return const RegistrationPage();
              }));
            },
          ),
          ListTile(
            title: const Text('Log Out'),
            onTap: () {
              Navigator.pushAndRemoveUntil(context,
                  MaterialPageRoute(builder: (context) {
                return const LoginPage();
              }), (route) => false);
            },
          ),
        ],
      ),
    );
  }
}
