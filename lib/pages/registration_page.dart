import 'package:flutter/material.dart';
import 'package:notebook/databasehelper/databasehelper.dart';
import 'package:notebook/models/userinfo.dart';
import 'package:notebook/pages/widgets/custombutton_widgets.dart';

class RegistrationPage extends StatefulWidget {
  const RegistrationPage({Key? key}) : super(key: key);

  @override
  State<RegistrationPage> createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  TextEditingController? _userNameTxtClt;
  TextEditingController? _userEmailTxtClt;
  TextEditingController? _userPasswordTxtClt;
  TextEditingController? _userConPasswordTxtClt;
  GlobalKey<FormState>? formKey;
  DatabaseHelper? db;

  @override
  void initState() {
    _userNameTxtClt = TextEditingController();
    _userEmailTxtClt = TextEditingController();
    _userPasswordTxtClt = TextEditingController();
    formKey = GlobalKey<FormState>();
    db = DatabaseHelper();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Registration Page'),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.arrow_back,
          ),
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(5),
        child: Container(
          margin: const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
          //padding: EdgeInsets.all(5),
          child: Form(
            key: formKey,
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Row(
                  children: const [
                    Text(
                      'Full  Name',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      ' *',
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.red),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 5,
                ),
                Padding(
                  padding: EdgeInsets.zero,
                  child: TextFormField(
                    controller: _userNameTxtClt,
                    keyboardType: TextInputType.text,
                    obscureText: false,
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return 'Please Provide  Your Name!';
                      }
                    },
                    decoration: InputDecoration(
                      prefixIcon: const Icon(Icons.man_outlined),
                      filled: true,
                      fillColor: Colors.grey.shade300,
                      border: const OutlineInputBorder(
                          borderSide: BorderSide(
                        color: Colors.blue,
                      )),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 15,
                ),
                Row(
                  children: const [
                    Text(
                      'Email',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      ' *',
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.red),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 5,
                ),
                Padding(
                  padding: EdgeInsets.zero,
                  child: TextFormField(
                    controller: _userEmailTxtClt,
                    keyboardType: TextInputType.emailAddress,
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return "Please Provide Your Email!";
                      } else {}
                    },
                    decoration: InputDecoration(
                      prefixIcon: const Icon(Icons.email_outlined),
                      filled: true,
                      fillColor: Colors.grey.shade300,

                      // errorStyle: TextStyle(
                      //   fontSize: 11,
                      //   height: 0.3,
                      // ),

                      border: const OutlineInputBorder(
                          borderSide: BorderSide(
                        color: Colors.blue,
                      )),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 15,
                ),
                Row(
                  children: const [
                    Text(
                      'Password',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      ' *',
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.red),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 5,
                ),
                Padding(
                  padding: EdgeInsets.zero,
                  child: TextFormField(
                    controller: _userPasswordTxtClt,
                    keyboardType: TextInputType.text,
                    obscureText: true,
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return "Please Provide Password!";
                      } else {}
                    },
                    decoration: InputDecoration(
                      prefixIcon: const Icon(Icons.password_outlined),
                      filled: true,
                      fillColor: Colors.grey.shade300,
                      border: const OutlineInputBorder(
                          borderSide: BorderSide(
                        color: Colors.blue,
                      )),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 15,
                ),
                Row(
                  children: const [
                    Text(
                      'Confirm Password',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      ' *',
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.red),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 5,
                ),
                Padding(
                  padding: EdgeInsets.zero,
                  child: TextFormField(
                    controller: _userConPasswordTxtClt,
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return "Please Provide Confirm Password!";
                      }
                      if (value != _userPasswordTxtClt!.text) {
                        return "Confirm Password is not match!";
                      } else {}
                    },
                    decoration: InputDecoration(
                      prefixIcon: const Icon(Icons.password_outlined),
                      filled: true,
                      fillColor: Colors.grey.shade300,
                      border: const OutlineInputBorder(
                          borderSide: BorderSide(
                        color: Colors.blue,
                      )),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: EdgeInsets.zero,
                  child: CustomButton(onPressed: () async {
                    if (formKey!.currentState!.validate()) {
                      UserInfo user = UserInfo(
                        name: _userNameTxtClt!.text,
                        username: _userEmailTxtClt!.text,
                        password: _userPasswordTxtClt!.text,
                      );

                      bool status =
                          await db!.isUserExist(_userEmailTxtClt!.text);
                      if (status == true) {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text('This Email already exist')));
                      } else {
                        int userSave = await db!.addUser(user);
                        if (userSave > 0) {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              content: Text('Successfully Registered')));
                        }
                        Navigator.pop(context);
                      }
                    }
                  }),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
