import 'package:flutter/material.dart';
import 'package:notebook/models/notebook.dart';
import 'package:notebook/pages/drawer/drawer_page.dart';
import 'package:notebook/pages/note_add_page.dart';
import 'package:notebook/pages/util/util.dart';
import 'package:notebook/provider/notebooke_provider.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Utility utility = Utility();
  NoteBookProvider? nProvider;

  @override
  void initState() {
    super.initState();
    nProvider = NoteBookProvider();
    fetchNoteList();
  }

  void fetchNoteList() {
    try {
      nProvider!.fetchNote();


      //  notebookListContainer!.clear();
      //  notebookListContainer = await nProvider!.fetchNote();
      // if (notebookListContainer!.isNotEmpty) {
      //   setState(() {
      //     notebookList = notebookListContainer;
      //   });
      // }
    } catch (er) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(er.toString())));
    }
  }

  void filtering(String? query) {
    nProvider!.filterSearchResult(query);
  }

  void updateNote(int itemId) async {
    //print(itemId);
    await Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (
      context,
    ) {
      return NoteAddPage(
        itemId: itemId,
      );
    }), (route) => false);
  }

  void deleteNote(int itemId) async {
    String result = await nProvider!.deleteNoteById(itemId);
    if (result == "Success") {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Delete Successfully!')));
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Data is notDeleted!')));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<NoteBookProvider>(builder: (
      _,
      provider,
      ___,
    ) {
      return nProvider != null
          ? Scaffold(
              appBar: AppBar(
                title: Text("Note Book(${nProvider!.notebookList.length})"),
              ),
              floatingActionButton: Container(
                margin: const EdgeInsets.only(right: 30, bottom: 30),
                child: FloatingActionButton(
                  onPressed: () async {
                    bool? isAdded = await Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return const NoteAddPage();
                    }));
                    if (isAdded != null) {
                      if (isAdded == true) {
                        //notebookList = [];

                        fetchNoteList();
                      }
                    }
                  },
                  child: const Icon(Icons.add),
                ),
              ),
              drawer: const DrawerPage(),
              body: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 12.0, horizontal: 12),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          //margin: const EdgeInsets.all(10),
                          child: const Icon(
                            Icons.note_sharp,
                            size: 40,
                          ),
                        ),
                        const Text(
                          'Hello',
                          style: TextStyle(fontSize: 25),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        const Text(
                          'Rabiul',
                          style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Text(
                          utility.greeting().toString(),
                          style: const TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      height: 50,
                      // margin: const EdgeInsets.only(
                      //   top: 15,
                      //   bottom: 15,
                      // ),

                      child: TextField(
                        onChanged: (newChar) {
                          //filtering(newChar);
                          setState(() {
                            nProvider!.filterSearchResult(newChar);
                          });
                        },
                        decoration: InputDecoration(
                          hintText: "Search by title",
                          // labelText: "Search here",
                          prefixIcon: Icon(Icons.search),
                          filled: true,
                          fillColor: Colors.grey.shade300,
                          // border: InputBorder.none,

                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.grey.shade300,
                              ),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Expanded(
                      child: ListView.separated(
                        separatorBuilder: (context, index) {
                          return SizedBox(
                            height: 10,
                          );
                        },
                        itemCount: nProvider!.notebookList.length,
                        itemBuilder: (context, index) {
                          NoteBook noteList = nProvider!.notebookList[index];
                          int itemId = nProvider!.notebookList[index].id;
                          print(itemId);
                          return Container(
                            //margin: EdgeInsets.only(top: 5),
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Colors.blue,
                              ),
                              borderRadius: BorderRadius.circular(5),
                              color: Colors.white,
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          noteList.title,
                                          //noteList.id.toString(),
                                          style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                        Text(
                                          noteList.content,
                                          style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.normal,
                                          ),
                                        ),
                                        Text(
                                          noteList.date,
                                          style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.normal,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  PopupMenuButton<String>(
                                      padding: EdgeInsets.zero,
                                      icon: Icon(Icons.more_vert),
                                      onSelected: (value) async {},
                                      itemBuilder: (context) {
                                        return <PopupMenuEntry<String>>[
                                          PopupMenuItem<String>(
                                            value: 'edit',
                                            child: ListTile(
                                              leading: Icon(Icons.edit),
                                              title: Text('Edit'),
                                              onTap: () {
                                                //updateNote(itemId);
                                                //Navigator.pop(context);
                                              },
                                            ),
                                          ),
                                          PopupMenuItem<String>(
                                            value: 'delete',
                                            child: ListTile(
                                              leading: Icon(Icons.delete),
                                              title: Text('Delete'),
                                              onTap: () {
                                                deleteNote(itemId);
                                                Navigator.pop(context);
                                                fetchNoteList();
                                              },
                                            ),
                                          ),
                                        ];
                                      })
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            )
          : Scaffold(
              appBar: AppBar(
                title: const Text("Note Book()"),
              ),
              floatingActionButton: Container(
                margin: const EdgeInsets.only(right: 30, bottom: 30),
                child: FloatingActionButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return const NoteAddPage();
                    }));
                  },
                  child: const Icon(Icons.add),
                ),
              ),
              drawer: const DrawerPage(),
            );
    });
  }
}
