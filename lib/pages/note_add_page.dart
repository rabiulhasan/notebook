import 'package:flutter/material.dart';
import 'package:notebook/models/notebook.dart';
import 'package:notebook/pages/widgets/custombutton_widgets.dart';
import 'package:notebook/provider/notebooke_provider.dart';
import 'package:provider/provider.dart';

class NoteAddPage extends StatefulWidget {
  final int? itemId;

  const NoteAddPage({this.itemId, Key? key}) : super(key: key);

  @override
  State<NoteAddPage> createState() => _NoteAddPageState();
}

class _NoteAddPageState extends State<NoteAddPage> {
  NoteBookProvider? nProvider;
  TextEditingController? _noteTitleClt;
  TextEditingController? _noteContentClt;
  String? _mDate;
  int? itemId;

  @override
  void initState() {
    super.initState();
    nProvider = NoteBookProvider();
    _noteTitleClt = TextEditingController();
    _noteContentClt = TextEditingController();
    _mDate = DateTime.now().toString().substring(0, 10);
    itemId = itemId;
  }

  void addNoteHelper() async {
    NoteBook noteBook = NoteBook(
      title: _noteTitleClt!.text,
      content: _noteContentClt!.text,
      date: _mDate,
    );
    String result = await nProvider!.insertNote(noteBook);
    if (result == "Success") {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Note Successfully Added')));

      Navigator.pop(context);
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Can not added')));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<NoteBookProvider>(builder: (
      _,
      noteProvider,
      ___,
    ) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.amber,
          title: const Text('Note Book'),
          leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(
                Icons.arrow_back,
                size: 30,
                color: Colors.black,
              )),
        ),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(
                  'Note Title:',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(itemId.toString()),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: TextField(
                  controller: _noteTitleClt,
                  onChanged: (value) {},
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.grey.shade300,
                    // border: InputBorder.none,
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.grey,
                      ),
                    ),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey)),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(
                  'Note Content:',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: TextField(
                  controller: _noteContentClt,
                  onChanged: (value) {},
                  maxLines: 5,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.grey.shade300,
                    // border: InputBorder.none,
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.grey,
                      ),
                    ),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey)),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(
                  'Note Date:',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Container(
                  color: Colors.grey.shade300,
                  child: ListTile(
                    title: Text(_mDate!),
                    onTap: () {
                      showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime(2000),
                        lastDate: DateTime(2030),
                      ).then((DateTime? newdate) {
                        if (newdate != null) {
                          setState(() {
                            _mDate = newdate.toString().substring(0, 10);
                          });
                        }
                      });
                    },
                    trailing: Icon(Icons.calendar_month_rounded),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: CustomButton(
                  onPressed: () {
                    if (_noteTitleClt!.text.isEmpty) {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text('Please Provide Note title'),
                      ));
                    } else if (_noteContentClt!.text.isEmpty) {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text('Please Provide Note Content')));
                    } else {
                      noteProvider.isLoading;

                      addNoteHelper();
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      );
    });
  }
}
