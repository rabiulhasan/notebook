import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:notebook/models/notebook.dart';
import 'package:notebook/repository/notebook_repository.dart';

class NoteBookProvider with ChangeNotifier {
  final noteBookRepo = NoteBookRepository();
  bool? _isLoading = false;
  List<NoteBook>? _notebookList;
  List<NoteBook>? _notebookListContainer;

  bool get isLoading => _isLoading!;

  set isLoading(value) {
    _isLoading = value;
    return notifyListeners();
  }

  List<NoteBook> get notebookListContainer => _notebookListContainer!;

  set notebookListContainer(List<NoteBook> value) {
    _notebookListContainer = value;
    notifyListeners();
  }

  List<NoteBook> get notebookList => _notebookList!;

  set notebookList(List<NoteBook> value) {
    _notebookList = value;
    notifyListeners();
  }

  Future<String> insertNote(NoteBook noteBook) async {
    String status = "loading";
    _isLoading = true;
    //return await noteBookRepo.insertNote(noteBook);
    int isRowAffected = await noteBookRepo.insertNote(noteBook);
    if (isRowAffected > 0) {
      return "Success";
    } else {
      return "Failed";
    }
    _isLoading = false;
    return status;
  }

  Future<void> fetchNote() async {
    _isLoading = true;
    notebookListContainer = await noteBookRepo.fetchNote();
    if (notebookListContainer.isNotEmpty) {
      notebookList = notebookListContainer;
      // print(notebookList.length);
    } else {
      notebookList = [];
    }

    _isLoading = false;
  }

  Future<String> updateNote(NoteBook noteBook) async {
    String status = "loading";
    _isLoading = true;
    int isUpdate = await noteBookRepo.updateNote(noteBook);
    if (isUpdate > 0) {
      return "Success";
    } else {
      return "Failed";
    }
    _isLoading = false;
    return status;
  }

  Future<String> deleteNote(NoteBook noteBook) async {
    String status = "loading";
    _isLoading = true;
    int isDelete = await noteBookRepo.deleteNote(noteBook);
    if (isDelete > 0) {
      return "Success";
    } else {
      return "Failed";
    }
    _isLoading = false;
    return status;
  }

  Future<String> deleteNoteById(int id) async {
    String status = "loading";
    _isLoading = true;
    int isDelete = await noteBookRepo.deleteNoteById(id);
    if (isDelete > 0) {
      return "Success";
    } else {
      return "Failed";
    }
    _isLoading = false;
    return status;
  }

  Future<String> deleteTable(String tableName) async {
    String status = "loading";
    _isLoading = true;
    int isDelete = await noteBookRepo.deleteTable(tableName);
    if (isDelete > 0) {
      return "Success";
    } else {
      return "Failed";
    }
    _isLoading = false;
    return status;
  }

  void filterSearchResult(String? query) {
    if (query!.isEmpty) {
      notebookList = notebookListContainer;
    } else {
      List<NoteBook> mList = [];
      for (NoteBook book in notebookListContainer) {
        if (book.title.toString().toLowerCase().contains(query.toLowerCase())) {
          mList.add(book);
        }
      }
      notebookList = mList;
    }
  }
}
